<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePendaftaranTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pendaftaran', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->nama_lengkap();
            $table->tetala();
            $table->alamat();
            $table->asal_sekolah();
            $table->jenis_kelamin();
            $table->nama_wali();
            $table->tetala();
            $table->alamat();
            $table->pekerjaan();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pendaftaran');
    }
}
